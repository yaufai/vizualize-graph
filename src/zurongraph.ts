import { ZuronEdge } from "./zuronedge"
import { ZuronNode } from "./zuronnode"
import * as math from 'mathjs'

let jsdom = require('jsdom')
const { JSDOM } = jsdom

export class ZuronGraph {
    nodes : ZuronNode[]
    edges : ZuronEdge[]
    reservedEdges : ReservedEdge[]
    adjacencyMatrix : number[][]
    properties : {[key: string]: string}

    constructor(nodes: ZuronNode[], edges: ZuronEdge[], properties: {[key: string]: any}) {
        this.properties = properties        
        this.nodes = [], this.edges = []
        this.reservedEdges = []
        this.adjacencyMatrix = []
        for (let node of nodes) {
            this.addNode(node)
        }

        for (let edge of edges) {
            this.addEdge(edge)
        }
    }

    getProperty(key: string) : any {
        return this.properties[key]
    }

    setProperty(key: string, value: any) : void {
        this.properties[key] = value
    }

    addNode(node: ZuronNode) : void {
        if (this.existsNodeId(node.nodeId)) {
            //TODO: raise error
            console.log("Warn: Duplicated Node ID: ", node.nodeId)
        } else {
            this.nodes.push(node)
            for (let adjacency of this.adjacencyMatrix) {
                adjacency.push(0)
            }

            let emptyList : number[] = Array<number>(this.nodes.length)
            this.adjacencyMatrix.push(emptyList.map(x => 0))
        }
        this.fulfillReservation()
    }

    addEdge(edge: ZuronEdge) : void {
        if (this.existsNodeId(edge.parent.nodeId) && this.existsNodeId(edge.child.nodeId)) {
            this.edges.push(edge)
            let indexParent : string = this.indexOfNode(edge.parent)
            let indexChild  : string = this.indexOfNode(edge.child)
            this.adjacencyMatrix[indexParent][indexChild] = 1
        } else {
            //TODO: raise error
        }

    }

    getNodeByName(nodeName: string) : ZuronNode {
        for (let node of this.nodes) {
            if (node.nodeName == nodeName) {
                return node
            }
        }

        return undefined
    }

    existsNodeId(nodeId: number) : boolean {
        return this.nodes.filter((value)=> {
            return value.nodeId == nodeId
        }).length > 0
    }

    existsNodeName(nodeName: string) : boolean {
        let a =  this.nodes.filter((value)=> {
            return value.nodeName == nodeName
        })
        return a.length > 0
    }

    findNodeByName(nodeName: string) : ZuronNode {
        return this.nodes.filter((value) => {
            return value.nodeName == nodeName
        })[0]
    }
    
    // the index of node (mainly ) is defined by the index in nodes
    indexOfNode(node: ZuronNode) : string {
        for (let index in this.nodes) {
            if (this.nodes[index].nodeId == node.nodeId) {
                return index
            }
        }
    }

    createPathCountMatrix(pathLength: number) : math.matrix {
        if (pathLength <= 1 ) {
            return math.matrix(this.adjacencyMatrix)
        } else {
            return math.multiply(math.matrix(this.adjacencyMatrix), this.createPathCountMatrix(pathLength - 1))
        }
    }

    reserveEdgeByName(parentName: string, childName: string, properties: {[key: string]: any}) : void {
        let reservedEdge : ReservedEdge = new ReservedEdge(parentName, childName, properties)
        this.reservedEdges.push(reservedEdge)
        this.fulfillReservation()
    }

    obtainNodesFlowInTo(parentNode: ZuronNode) : ZuronNode[] {
        let edgesFromNode : ZuronEdge[] = this.edges.filter((value) => {
            return value.parent.nodeName == parentNode.nodeName
        })

        return edgesFromNode.map((value) => {
            return value.child
        })
    }

    obtainNodesFlowOutFrom(childNode: ZuronNode) : ZuronNode[] {
        let edgesToNode : ZuronEdge[] = this.edges.filter((value) => {
            return value.child.nodeName == childNode.nodeName
        })

        return edgesToNode.map((value) => {
            return value.parent
        })
    }

    extractSubgraphContaining(centralNode: ZuronNode) : ZuronGraph {
        let graph : ZuronGraph = new ZuronGraph([], [], {})

        graph.addNode(centralNode)

        let dfsForAncestors : Function = (node: ZuronNode) => {
            this.obtainNodesFlowInTo(node).forEach((value) => {
                if (!graph.existsNodeName(value.nodeName)) {
                    graph.addNode(value)
                    graph.addEdge(new ZuronEdge(node, value, {}))
                    
                    dfsForAncestors(value)
                } else {
                    graph.reserveEdgeByName(node.nodeName, value.nodeName, {})
                }
            })
        }

        let dfsForDescendants : Function = (node: ZuronNode) => {
            this.obtainNodesFlowOutFrom(node).forEach((value) => {
                if (!graph.existsNodeName(value.nodeName)) {
                    graph.addNode(value)
                    graph.addEdge(new ZuronEdge(value, node, {}))
                    dfsForDescendants(value)
                } else {
                    graph.reserveEdgeByName(value.nodeName, node.nodeName, {})
                }
            })
        }
        dfsForAncestors(centralNode)
        dfsForDescendants(centralNode)
        
        return graph
    }
    
    fulfillReservation() : void {
        // let remainingReservations : ReservedEdge[] = this.reservedEdges.filter((value)=>{
        //     return !(this.existsNodeName(value.parentName) && this.existsNodeName(value.childName))
        // }) 
        // let fulfilledReservations : ReservedEdge[] = this.reservedEdges.filter((value=>{
        //     return this.existsNodeName(value.parentName) && this.existsNodeName(value.childName)
        // }))

        let remainingReservations : ReservedEdge[] = []
        let fulfilledReservations : ReservedEdge[] = []

        this.reservedEdges.forEach((reservation: ReservedEdge) => {
            // console.log(reservation, ":<br>")
            // console.log("-> ", reservation.parentName, this.existsNodeName(reservation.parentName) ,"<br>")
            // console.log("-> ", reservation.childName, this.existsNodeName(reservation.childName) ,"<br>")
            // console.log("-> ", this.existsNodeName(reservation.parentName) && this.existsNodeName(reservation.childName) ,"<br>")
            if (this.existsNodeName(reservation.parentName) && this.existsNodeName(reservation.childName)) {
                let parent : ZuronNode = this.findNodeByName(reservation.parentName)
                let child  : ZuronNode = this.findNodeByName(reservation.childName)
                this.addEdge(new ZuronEdge(parent, child, reservation.properties))
            } else {
                remainingReservations.push(reservation)
            }
        })
        
        // fulfilledReservations.forEach((value=>{
        //     let parent : ZuronNode = this.findNodeByName(value.parentName)
        //     let child  : ZuronNode = this.findNodeByName(value.childName)
        //     this.addEdge(new ZuronEdge(parent, child, value.properties))
        // }))

        this.reservedEdges = remainingReservations
    }

    getNodesThatSatisfies(callback: (node: ZuronNode) => boolean) : ZuronNode[] {
        return this.nodes.filter((value) => callback(value))
    }

    visualizeByDot() : string {
        let nodes : string = 'node [shape=box]\n' + this.nodes.map((value) => {
            return '"' + value.nodeName + '" [label="' + value.nodeName +  '"]\n'
        }).join("")

        let edges : string = this.edges.map((value) => {
            return '"' + value.parent.nodeName + '" -> "' + value.child.nodeName + '"\n'
        }).join("")

        return "digraph transition {" + nodes + edges + "}"
    }
}


export class ReservedEdge {
    parentName : string
    childName  : string
    properties : {[key: string]: any}
    
    constructor(parentName: string, childName: string, properties : {[key: string]: any}) {
        this.parentName = parentName
        this.childName  = childName
        this.properties = properties
    }

}