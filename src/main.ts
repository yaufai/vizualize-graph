import { createTable, createStandAloneCss } from "./htmlrender"

import * as PegParser from "./parser"
import fs = require('fs')
import { ZuronGraph } from "./zurongraph";
import { ZuronNode } from "./zuronnode";
import { GRAPH_DEFAULT_NODE, GRAPH_FLOW, NODE_PROPERTIES, NODE_PHASE } from "./constants"
import { renderNode } from "./renderForMd";


function evaluateVariables(value: string, node: ZuronNode): string {
    return value.replace("($nodeName)", node.nodeName)
}

let parseContent = function(content : string): ZuronGraph {
    let graph : ZuronGraph = new ZuronGraph([], [], {})
    let parseResult = PegParser.parse(content)
    
    // Initial config parsing
    for (let block of parseResult) {
        if (block[0] == "Flow") {
            graph.setProperty(GRAPH_FLOW, block[1])
        } else if (block[0] == "Default") {
            graph.setProperty(GRAPH_DEFAULT_NODE, createDefaultNode(block[1][1], block[2][1]))
        }
    }

    let incrementedNodeId: number = 0
    // Create node objects
    for (let block of parseResult) {
        if (block[0] == "Node") {
            let node : ZuronNode = new ZuronNode(incrementedNodeId, block[1], {})
            incrementedNodeId++
            // Node Info
            for (let config of block[2][1]) {
                node.setProperty(config[0],config[1])
            }
            NODE_PROPERTIES.forEach((value)=> {
                if (node.getProperty(value) == undefined && graph.getProperty(GRAPH_DEFAULT_NODE).getProperty(value) != undefined) {
                    let defaultProperty : string = graph.getProperty(GRAPH_DEFAULT_NODE).getProperty(value)
                    node.setProperty(value, evaluateVariables(defaultProperty, node))
                }
            })
            graph.addNode(node)
            // Edges
            for (let edgeBlock of block[3][1]) {
                graph.reserveEdgeByName(node.nodeName, edgeBlock[1], {"action": edgeBlock[0] })
            }
        }
    }
    return graph
}

function createDefaultNode(nodeInfo : Array<Array<string>>, edges : Array<Array<string>>) : ZuronNode {
    let defaultNode : ZuronNode = new ZuronNode(undefined, undefined, {})
    for (let config of nodeInfo) {
        defaultNode.setProperty(config[0], config[1])
    }
    return defaultNode
}

// Prepare
let outputHtml : string = ''
let vueCode    : string = fs.readFileSync("vuecode.js", "utf8")

// Create the transition graph from the 
let transitionGraph : ZuronGraph = parseContent(fs.readFileSync("investigation.txt", "utf8"))

// Visualize the graph as a table
outputHtml = '[toc]\n' + fs.readFileSync('summary.md')
for (let phase of transitionGraph.getProperty(GRAPH_FLOW)) {
    outputHtml = outputHtml + '\n# ' + phase
    for (let node of transitionGraph.getNodesThatSatisfies((node)=> {
        return node.getProperty(NODE_PHASE) == phase
    })) {
        outputHtml = outputHtml + renderNode(node, transitionGraph)
    }
}


fs.writeFileSync('Result.md', outputHtml)

transitionGraph.nodes.forEach((value) => {
    let dot : string = transitionGraph.extractSubgraphContaining(value).visualizeByDot()
    let fileName : string = 'dot/graph_' + value.nodeName + '.dot'
    fs.writeFileSync(fileName, dot)
})

fs.writeFileSync('dot/graph_overAll.dot', transitionGraph.visualizeByDot())