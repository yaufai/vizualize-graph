import { ZuronNode } from "./zuronnode"
export class ZuronEdge {
    parent : ZuronNode
    child  : ZuronNode
    properties : {[key: string]: string}

    constructor(parent: ZuronNode, child: ZuronNode, properties: {[key: string]: string}) {
        this.parent = parent
        this.child  = child
        this.properties = properties
    }

    getProperty(key: string) : any {
        return this.properties[key]
    }
    
    setProperty(key: string, value: string) : void {
        this.properties[key] = value
    }
}