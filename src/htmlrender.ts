import { ZuronGraph } from "./zurongraph"
import { ZuronEdge } from "./zuronedge";
import { NODE_IMAGE_PATH, NODE_TEXT, GRAPH_FLOW, NODE_PHASE, NODE_STYLE, NODE_PROPERTIES, NODE_FILE_NAME, NODE_DESCRIPTION } from "./constants";
import { ZuronNode } from "./zuronnode";
let fs = require('fs');

const CLASS_EDGE_BOX           : string = 'edge_box'
const CLASS_DECORATOR_LIST_DOT : string = 'decorator_list_dot'
const CLASS_DECORATOR_ARROW    : string = 'decorator_arrow'
const CLASS_RAW_OF_PHASES      : string = 'RawOfPhases'
const CLASS_PHASE_NAME         : string = "PhaseName"
const CLASS_NODE_CELL          : string = "NodeCell"
const CLASS_RAW_FOR_NODES      : string = "RawOfNodeCells"
const CLASS_GRAPH_TABLE        : string = "Table"
const CLASS_NODE_PROPERTIES    : string = "nodeProperties"


export const DOT   : string = createTag("spam", "・", {"class": CLASS_DECORATOR_LIST_DOT})
export const ARROW : string = createTag("span", "->", {"class": CLASS_DECORATOR_ARROW})

export const CLASS_EDGE_ACTION = "edge_action"


function createPopUpLink(target : string)  : string {
    let classMap = {"v-on:click" : "zoomByLink", "target" : "img/" + target + ".png"} 
    return createTag("a", target, classMap)
}

// Receive an edge object and return this representation.
export function renderEdge(edge : ZuronEdge) : string {
    let content = DOT + createTag("span", edge.getProperty("action") , {"class": CLASS_EDGE_ACTION}) + "<br />" + ARROW + createPopUpLink(edge.child.nodeName)
    return createTag("div", content, {"class": CLASS_EDGE_BOX})
}

// Receive a node object with its nodeStyle thumbnail, and create a div tag for representing the node.
// The function itself does not perform type checking.
function renderScreenNode(node : ZuronNode) : string {
    let createTagForNodeProperties : Function = (node: ZuronNode) : string => {
        let fileName: string = node.getProperty(NODE_FILE_NAME)
        let description: string = node.getProperty(NODE_DESCRIPTION)
        let outputTag: string = ""
        if (fileName != undefined) {
            outputTag = outputTag + '<div>File: {{file}}</div>'.replace('{{file}}', fileName)
        }
        if (description != undefined) {
            outputTag = outputTag + '<div>Description: {{description}}</div>'.replace('{{description}}', description)
        }
        return createTag('div', outputTag, {'class': CLASS_NODE_PROPERTIES })
    }

    let properties : string = createTagForNodeProperties(node)
    

    // not self-contained
    return `
    <div class='thumbnailNode'>
        <div class='nodeName'>{{nodeName}}</div>
        <div class='nodeThumbnail'><img src='{{imagePath}}' v-on:click='zoomByEvent'></div>
        <div class='nodeThumbnail'><img src='{{transition}}'></img></div>
        <div>{{nodeProperties}}</div>
    </div>`.replace('{{nodeName}}', node.nodeName)
           .replace('{{imagePath}}', node.getProperty(NODE_IMAGE_PATH))
           .replace('{{transition}}', 'dotimage/graph_' + node.nodeName + '.png')
           .replace('{{nodeProperties}}', properties)
}

// Receive a node object with its nodeStyle text, acnd create a div tag for representing the node.
// The function itself does not perform type checking.
export function renderTextNode(node : ZuronNode) {
    let temp : string = `
    <div class='textNode'>
        <div class='nodeName'>{{nodeName}}</div>
        <div class='nodeText'>{{text}}</div>
    </div>
    `
    temp = temp.replace('{{nodeName}}', node.nodeName)
    temp = temp.replace('{{text}}', node.getProperty(NODE_TEXT))
    return temp
}

export function renderNode(node: ZuronNode, graph: ZuronGraph) : string {
    let edgeTags : string = renderEdgeTagsForNode(node, graph)
    if (node.getProperty(NODE_STYLE) == "thumbnail") {
        return renderScreenNode(node) + edgeTags
    } else if (node.getProperty(NODE_STYLE) == "text") {
        return renderTextNode(node)   + edgeTags
    } else {
        console.log("Unknown node style : " + node.getProperty(NODE_STYLE))
    }
}

export function renderEdgeTagsForNode(node: ZuronNode, graph: ZuronGraph) : string {
    let applicableEdges : ZuronEdge[] = graph.edges.filter((value) => {
        return value.parent.nodeName == node.nodeName
    })
    
    let tags : string[] = applicableEdges.map((value)=> {
        return renderEdge(value)
    })

    return tags.join("")
}

function createTag(tagname: string, content: string, attributes : {[key: string]: string }) : string{
    var attributePart : string = ""
    for (var key in attributes){
        attributePart += " " + key + "='" + attributes[key] + "'"
    }
    return "<" + tagname + attributePart + ">" + content + "</" + tagname + ">"
}

export function createTable(graph : ZuronGraph) : string {
    let biggestNumberOfNodes     : number   = 0
    let numberOfNodesInEachPhase : {[key: string]: number} = {}

    // let isPhaseOfNodeEqualTo : (ZuronNode, string) => boolean = (node: ZuronNode, phase: string) => {
    //     return node.getProperty(NODE_PHASE) == phase
    // }

    let nodesForEach              : {[key: string]: ZuronNode[]} = {}
    let renderedNodesForEachPhase : {[key: string]: string[]}    = {}
    graph.getProperty(GRAPH_FLOW).forEach(phase => {
        // List of nodes in each phase
        nodesForEach[phase] = graph.getNodesThatSatisfies((node: ZuronNode) => {
            return node.getProperty(NODE_PHASE) == phase
        })


        // List of the number of nodes in each phase
        numberOfNodesInEachPhase[phase] = nodesForEach[phase].length
        if (biggestNumberOfNodes < nodesForEach[phase].length) {
            biggestNumberOfNodes = nodesForEach[phase].length
        }

        // List of rendered nodes for each phase
        renderedNodesForEachPhase[phase] = nodesForEach[phase].map((node: ZuronNode) => {
            return renderNode(node, graph)
        })
    })


    let outputHtml : string = createTag('tr', graph.getProperty(GRAPH_FLOW).map((value)=> {
        return createTag('td', value, {})
    }).join(""), {})
    let rawHtml    : string
    for (let i = 0; i < biggestNumberOfNodes; i++) {
        rawHtml = ""
        graph.getProperty(GRAPH_FLOW).forEach(phase => {
            if (i < numberOfNodesInEachPhase[phase]) {
                rawHtml = rawHtml + createTag("td", renderedNodesForEachPhase[phase][i], {"class": CLASS_NODE_CELL})
            } else {
                rawHtml = rawHtml + createTag("td", "", {"class": CLASS_NODE_CELL})
            }
        })

        outputHtml = outputHtml + createTag("tr", rawHtml, {"class": CLASS_RAW_FOR_NODES})
        
    }

    return createTag("table", outputHtml, {"class":CLASS_GRAPH_TABLE})
}

export function createStandAloneCss(filepath: string) : string {
    let cssStatements : string = fs.readFileSync(filepath)
    return cssStatements
}