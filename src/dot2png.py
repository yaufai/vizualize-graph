import os

dotDirectory = '/Users/yuki.a.hara/workspace/vizflow/src/dot/'
output = '/Users/yuki.a.hara/workspace/vizflow/src/dotimage/'
dots = os.listdir(dotDirectory)
for dot in dots:
    imagename = output + os.path.splitext(dot)[0] + ".svg"
    os.system("dot -Tsvg " + dotDirectory + dot +  " > " + imagename)
    