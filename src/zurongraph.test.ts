import assert from 'power-assert';
import { ZuronGraph, ReservedEdge } from './zurongraph';
import { ZuronNode } from './zuronnode';
import { ZuronEdge } from './zuronedge';

describe("ZuronGraph", () => {
    it("Test existsNodeName(nodeName: string)", () => {
        let node  : ZuronNode  = new ZuronNode(1, "Test Node", {})
        let graph : ZuronGraph = new ZuronGraph([node], [], {})
        assert.equal(graph.existsNodeName("Test Node"), true)
    })

    it("Test make reservation: reserveEdgeByName", ()=> {
        let parentName : string = "parent"
        let childName  : string = "child"
        let graph      : ZuronGraph = new ZuronGraph([], [], {})
        graph.reserveEdgeByName(parentName, childName, {})
        assert.deepEqual(graph.reservedEdges, [new ReservedEdge(parentName, childName, {})])
    })

    it("Test fulfillReservation", ()=> {
        let parentName : string = "parent"
        let childName  : string = "child"
        let parentNode : ZuronNode = new ZuronNode(1, parentName, {})
        let childNode  : ZuronNode = new ZuronNode(2, childName , {})
        let reservation: ReservedEdge = new ReservedEdge(parentName, childName, {})
        let nodes : ZuronNode[] = [parentNode, childNode]
        let graph : ZuronGraph  = new ZuronGraph([], [], {})
        graph.nodes = nodes; graph.reservedEdges = [reservation]
        graph.adjacencyMatrix = [[0,0],[0,0]]
        graph.fulfillReservation()
        assert.deepEqual(graph.reservedEdges, [])
        assert.deepEqual(graph.edges, [new ZuronEdge(parentNode, childNode, {})])
    })

    it("Test fulfillReservation at the time of making reservation", () => {
        let parentName : string = "parent"
        let childName  : string = "child"
        let parentNode : ZuronNode = new ZuronNode(1, parentName, {})
        let childNode  : ZuronNode = new ZuronNode(2, childName , {})
        let nodes : ZuronNode[] = [parentNode, childNode]
        let graph : ZuronGraph  = new ZuronGraph(nodes, [], {})
        graph.reserveEdgeByName(parentName, childName, {})
        assert.deepEqual(graph.reservedEdges, [])
        assert.deepEqual(graph.edges, [new ZuronEdge(parentNode, childNode, {})])
    })

    it("Test fulfillReservation at the time of adding a node", ()=> {
        let parentName : string = "parent"
        let childName  : string = "child"
        let parentNode : ZuronNode = new ZuronNode(1, parentName, {})
        let childNode  : ZuronNode = new ZuronNode(2, childName , {})

        // Only parentNode is included in the graph first
        let nodes : ZuronNode[] = [parentNode]
        let graph : ZuronGraph  = new ZuronGraph(nodes, [], {})
        graph.reserveEdgeByName(parentName, childName, {})

        // The reservation is still remaining
        assert.deepEqual(graph.reservedEdges.length, 1)
        assert.deepEqual(graph.edges, [])
        
        //
        graph.addNode(childNode)
        assert.deepEqual(graph.reservedEdges.length, 0)
        assert.deepEqual(graph.edges, [new ZuronEdge(parentNode, childNode, {})])
    })
});