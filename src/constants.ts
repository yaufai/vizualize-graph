
// Graph properties
export const GRAPH_FLOW : string = "Flow"
export const GRAPH_DEFAULT_NODE : string = "DefaultGraph"


// Node properties
export const NODE_IMAGE_PATH  : string = "image"
export const NODE_STYLE       : string = "node-style"
export const NODE_PHASE       : string = "phase"
export const NODE_FILE_NAME   : string = "file"
export const NODE_DESCRIPTION : string = "description"
export const NODE_TEXT        : string = "text"

export const NODE_PROPERTIES  : string[] = [
    NODE_STYLE,
    NODE_PHASE,
    NODE_IMAGE_PATH,
    NODE_FILE_NAME,
    NODE_TEXT
]