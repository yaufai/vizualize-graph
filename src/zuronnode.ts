export class ZuronNode {
    nodeId : number
    nodeName : string
    properties : {[key: string]: string}

    constructor(nodeId: number, nodeName: string, properties: {[key: string]: string}) {
        this.nodeId = nodeId
        this.nodeName = nodeName
        this.properties = properties
    }

    setProperty(key: string, value: string) : void {
        this.properties[key] = value
    }

    getProperty(key: string) : string {
        // Exception handling
        return this.properties[key]
    }
}