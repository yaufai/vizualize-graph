import { ZuronNode } from "./zuronnode";
import { NODE_FILE_NAME, NODE_DESCRIPTION, NODE_IMAGE_PATH } from "./constants";
import { ZuronGraph } from "./zurongraph";
import { ZuronEdge } from "./zuronedge";

const DOT   : string = createTag("spam", "・", {})
const ARROW : string = createTag("span", "->", {"style": 'font-family: "Fira Code";white-space: nowrap;'})

export function renderNode(node: ZuronNode, graph: ZuronGraph) : string {
    let structure : string = `\n## {{NodeName}}\n- FILE: {{FileName}}\n- DESCRIPTION:
    {{description}}\n\n### Screenshot
    <div><img src='{{screenshot}}' id='shot-{{NodeName}}'></div>\n\n### Transition
    <div><img src='{{transition}}'></div>
    {{edges}}
    `
    let filename : string = node.getProperty(NODE_FILE_NAME)
    
    let description: string = node.getProperty(NODE_DESCRIPTION)
    if (filename == undefined) { filename = 'None'}
    if (description == undefined) { description = 'None'}


    return structure.replace('{{NodeName}}', node.nodeName)
                    .replace('{{NodeName}}', node.nodeName)
                    .replace('{{FileName}}', filename)
                    .replace('{{description}}', description)
                    .replace('{{screenshot}}', node.getProperty(NODE_IMAGE_PATH))
                    .replace('{{transition}}', 'dotimage/graph_' + node.nodeName + '.svg')
                    .replace('{{edges}}', renderEdgeTagsForNode(node, graph))
}

export function renderEdgeTagsForNode(node: ZuronNode, graph: ZuronGraph) : string {
    let applicableEdges : ZuronEdge[] = graph.edges.filter((value) => {
        return value.parent.nodeName == node.nodeName
    })
    
    let tags : string[] = applicableEdges.map((value)=> {
        return renderEdge(value)
    })

    return tags.join("")
}

function createTag(tagname: string, content: string, attributes : {[key: string]: string }) : string {
    var attributePart : string = ""
    for (var key in attributes){
        attributePart += " " + key + "='" + attributes[key] + "'"
    }
    return "<" + tagname + attributePart + ">" + content + "</" + tagname + ">"
}

export function renderEdge(edge : ZuronEdge) : string {
    let action : string = createTag("span", edge.getProperty("action") , {})
    let screen : string = createPopUpLink(edge.child.nodeName)
    let content = DOT + action + "<br />" + ARROW + screen
    return createTag("div", content, {})
}

function createPopUpLink(target : string)  : string {
    let classMap = {"href": "#shot-" + target} 
    return createTag("a", target, classMap)
}
